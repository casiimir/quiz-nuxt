// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  typescript: {
    typeCheck: true
  },
  modules: [
    ['@nuxtjs/google-fonts', {
      families: {
        Lato: [400, 900],
        'Press Start 2P': [400]
      }
    }],
    ['@nuxtjs/robots', {
      UserAgent: '*',
      Disallow: ''
    }],
    '@pinia/nuxt',
    '@nuxt/image',
  ],
  css: [
    './assets/css/main.scss',
  ],
  app: {
    head: {
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
      title: 'Quiz Game',
      htmlAttrs: {
        lang: 'en'
      },
      meta: [
        {
          name: 'description',
          content: 'Join Quiz Game: Engage in Exciting Matches, Achieve Victory, and Surpass Expectations! Interactive Gaming Experience for All Ages.',
        },
      ]
    }
  },
})
