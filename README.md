# Casimiro Pietro Ciancimino - Fast Track Home Assignment

## Usage

Assuming you have `node.js` and `npm` installed on your system for a smooth setup process, follow these steps:

1. **Clone the Repository**: First, clone the project repository from GitLab.
<br />
`https://gitlab.com/casiimir/quiz-nuxt.git`
<br />

2. **Install Dependencies**: Navigate to the project directory and install the necessary dependencies.
<br />
`npm install`
<br />

3. **Run the Application**: Once the dependencies are installed, you can start the server.
<br />
`npm run dev`
<br />

This will launch the application locally on your machine.


## Design Assumptions

- The requirements clearly indicate a simple trivia-game style application, which currently does not require enriched and complex data, except for being scalable for future implementation without too many modifications.
- User interaction is limited to the game rules: a correct answer adds a score, a wrong answer retains the previous value. To this, I have added a functionality to calculate the user's final score percentile, based on the score for a correct answer multiplied by the time taken to respond.

## Scalable Architecture

1. Implementation of reusable components for possible future implementations.
2. Structure in Sass using variables and mixins in a CSS scoped style context.
3. Implementation of parallax in Composable, so that it can be reused in future components.
4. Media queries are used on the created components, but the structure could easily be inherited by others to maintain structural consistency.
5. Given the limited number of features provided, I confined myself to creating a single global source of truth with Pinia, which serves as the main engine of the entire application.
6. At the data level, I created a local mock with a structure that can be easily implemented in Rest API mode.
7. The application correctly follows the CI/CD pipeline, deployed on Vercel and hosted on GitLab, with the potential to contribute to the project efficiently and already set up.

## How to Use the Application

Using the Quiz Game Application is straightforward and engaging:

1. **Starting the Game**: From the splash screen, begin your quiz journey by entering your name to track your progress.
2. **Answering Questions**: You will be presented with multiple-choice questions. Select your answer from the provided options.
3. **Tracking Your Score**: After each question, your score is updated in real-time, reflecting your performance.
4. **Leaderboard**: After completing the quiz, view your rank on the leaderboard to see how you compare with other players.
5. **New Game**: To play again, simply restart the game from the main menu.

The intuitive design and user-friendly interface ensure a smooth and enjoyable quiz experience.

## Features

The Quiz Game Application boasts a variety of features designed to provide an immersive and educational experience:

- **Scoring**: Experience instant feedback with a comprehensive scoring system that keeps track of your progress at the end of the game.
- **Leaderboard Integration**: Compete with others and see where you stand globally with our integrated leaderboard system.
- **Cross-Platform Compatibility**: Enjoy the game on any device, thanks to our responsive design that adapts seamlessly to both mobile and desktop screens.
- **Engaging User Interface**: The application boasts a user-friendly interface, crafted meticulously to provide an intuitive gaming experience.
- **Parallax Effect**: To make the user experience richer and more stimulating.

### Improvements

Possible improvements would primarily involve adding more features:

- A home page that could also serve as a landing page.
- Implementation of a remote server to rely on for data and information persistence (currently, each refresh clearly results in the loss of the previous state).
- Creation and division into pages to catalog and manage routes, search parameters, and possibly enrich the user experience as well.
- Implementation of remote challenges among various users.
- And so on, we could continue for quite a while...

## Architecture

In addition to Vue 3, Nuxt.js, Pinia, Figma, Lighthouse, and TypeScript, the project incorporates several plugins and tools to enhance its functionality and user experience:

- **Component-Based Architecture**: Leveraging Vue's component-based architecture for building reusable and encapsulated UI components.
- **State Management**: Utilizes Pinia for centralized and predictable state management.
- **Server-Side Rendering**: Enhanced SEO through server-side rendering, managed by Nuxt.js.
- **Responsive Design**: UI designed in Figma, ensuring optimal viewing on all devices.
- **Performance Optimization**: Incorporation of Nuxt/Image and other tools for efficient load times.
- **Scalability**: Supports feature and user load scalability.
- **Mock Data Integration**: Internal mock data used for question and answer management, with a structure that's easily adaptable to external data sources like REST APIs.

### Minor extensions
- **Sass**: A powerful CSS extension language, used to write more maintainable and structured styles.
- **Sass Loader**: Integrated with Webpack, enabling the compilation of Sass files into CSS.
- **Google Fonts**: Provides a diverse range of fonts, enhancing the visual appeal and readability of the application.
- **Nuxt/Image**: An optimization module for handling images, ensuring efficient loading and rendering.
- **Nuxtjs/Robots**: Utilized for managing the robots.txt file, aiding in search engine optimization.
