import { defineStore } from 'pinia';
import { questionsMock } from "../mocks";
import type { Choice, Question, UserAnswer, User } from '../types';

/**
 * Main store for managing questions, answers, and user interactions in the Quiz Game.
**/
export const useQuestAndAnswer = defineStore('questAndAnswer', {
  state: () => ({
    // Current question being displayed.
    actualQuestion: questionsMock.list[0] as Question,
    // Quiz properties.
    difficulty: questionsMock.difficulty,
    type: questionsMock.type,
    quantity: questionsMock.quantity,
    // Current user playing the quiz.
    currentUser: null as User | null,
    // Array of users who have played the quiz.
    users: [] as User[],
    // Current view of the application.
    viewRender: 'splash',
  }),
  actions: {
    /**
     * Handles the submission of an answer for a question.
     * @param {Choice} choice - The user's choice for the current question.
    **/
    onQuestionSubmit(choice: Choice) {
      if (!this.currentUser) {
        return;
      }
      const prevQuestion = this.actualQuestion.id;
      const correctChoice = this.actualQuestion.choices.find(c => c.isCorrect);
      const userAnswer = {
        id: this.currentUser.answers.length + 1,
        question: this.actualQuestion.question,
        answer: choice.value,
        correctAnswer: correctChoice ? correctChoice.value : undefined
      } as UserAnswer;

      if (prevQuestion === 1) {
        this.currentUser.startTime = new Date();
      }

      if (prevQuestion !== this.quantity) {
        this.currentUser.answers.push(userAnswer);
        this.actualQuestion = questionsMock.list[prevQuestion];
      } else {
        this.currentUser.endTime = new Date();
        this.currentUser.answers.push(userAnswer);
        this.users.push(this.currentUser);
        this.onChangeRender('score');
      }
      if (userAnswer.correctAnswer === userAnswer.answer) {
        this.currentUser.score++;
      }
    },
    /**
     * Evaluates and returns the final score of the current user.
     * @returns Final score of the user.
    **/
    onScoreEvaluate() {
      if (!this.currentUser) {
        return 0;
      }

      const getMilliseconds = Number(this.currentUser.endTime) - Number(this.currentUser.startTime);
      const getSeconds = ((getMilliseconds % 60000) / 1000);
      const finalScore = (this.currentUser.score / Number(getSeconds) * 1000).toFixed(0);

      this.currentUser.finalScore = Number(finalScore);
      return finalScore;
    },
    /**
     * Changes the current render view of the application.
     * @param {string} view - The view to render.
    **/
    onChangeRender(view: string) {
      this.viewRender = view;
    },
    /**
     * Starts a new game with a given user name.
     * @param {string} userName - Name of the user to start a new game.
    **/
    onStartNewGame(userName: string) {
      const newUser = {
        id: this.users.length + 1,
        name: userName,
        score: 0,
        finalScore: 0,
        startTime: new Date(),
        endTime: null,
        answers: [],
        imgUrl: `https://robohash.org/${userName}`
      };

      this.currentUser = newUser;
      this.actualQuestion = questionsMock.list[0];
      this.onChangeRender('questions');
    },
    /**
     * Calculates and returns the percentile rank of the current user's score.
     * @returns Percentile rank of the user's score.
    **/
    calculateUserScorePercentile() {
      if (!this.currentUser || this.users.length <= 1) {
        return "";
      }

      // Include current user in users array if not already included
      const usersWithCurrent = this.users.some(user => user.id === this.currentUser?.id)
        ? [...this.users]
        : [...this.users, this.currentUser];

      // Sort users by their finalScore in descending order
      const sortedUsers = usersWithCurrent.sort((a, b) => b.finalScore - a.finalScore);

      // Find current user's index
      const currentUserIndex = sortedUsers.findIndex(user => user.id === this.currentUser?.id);

      // Calculate percentile and return formatted string
      const percentile = ((currentUserIndex / sortedUsers.length) * 100).toFixed(0);
      return `You're in the top ${percentile}% of scores!`;
    }
  }
});
