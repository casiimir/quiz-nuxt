// useBackgroundScroll.ts
import { ref } from 'vue'

export function useBackgroundScroll() {
  const bodyEl = document.querySelector('body');
  const initialBgPosX = bodyEl?.style.backgroundPositionX.replace('px', '') || '0';
  const bodyBgPos = ref(parseInt(initialBgPosX));

  const updateBackgroundPosition = (offset: number) => {
    if (bodyEl) {
      bodyBgPos.value += offset;
      bodyEl.style.backgroundPositionX = `${bodyBgPos.value}px`;
    }
  };

  return { updateBackgroundPosition };
}
