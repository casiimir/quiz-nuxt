export interface Question {
  id: number;
  question: string;
  choices: Choice[];
  imgUrl: string;
}

export interface Choice {
  id: number;
  value: string;
  isCorrect: boolean;
}

export interface UserAnswer {
  id: number;
  question: string;
  answer: string;
  correctAnswer?: string;
}

export interface User {
  id: number;
  name: string;
  score: number;
  finalScore: number;
  startTime: Date | null;
  endTime: Date | null;
  answers: UserAnswer[];
  imgUrl: string;
}