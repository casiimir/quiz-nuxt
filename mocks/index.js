export const questionsMock = {
  id: 1,
  type: "space",
  difficulty: "easy",
  quantity: 5,
  list: [
    {
      id: 1,
      question: "Which planet is known for its spectacular rings?",
      imgUrl: "https://freepngimg.com/download/universe/72057-planet-planets-system-solar-icon-free-download-png-hd.png",
      choices: [
        { id: 1, value: "Mercury", isCorrect: false },
        { id: 2, value: "Saturn", isCorrect: true },
        { id: 3, value: "Neptune", isCorrect: false }
      ]
    },
    {
      id: 2,
      question: "What is the largest planet in the solar system?",
      imgUrl: "https://www.pngmart.com/files/22/Jupiter-PNG-Isolated-File.png",
      choices: [
        { id: 1, value: "Jupiter", isCorrect: true },
        { id: 2, value: "Earth", isCorrect: false },
        { id: 3, value: "Uranus", isCorrect: false }
      ]
    },
    {
      id: 3,
      question: "Which planet is known as the 'Red Planet'?",
      imgUrl: "https://www.pngall.com/wp-content/uploads/13/Mars-Planet-PNG-Cutout.png",
      choices: [
        { id: 1, value: "Mars", isCorrect: true },
        { id: 2, value: "Venus", isCorrect: false },
        { id: 3, value: "Pluto", isCorrect: false }
      ]
    },
    {
      id: 4,
      question: "Which planet has the shortest day in the solar system?",
      imgUrl: "https://cdn.iconscout.com/icon/free/png-256/free-jupiter-planet-astrology-bigest-solar-system-gas-giant-3-20856.png",
      choices: [
        { id: 1, value: "Jupiter", isCorrect: true },
        { id: 2, value: "Mercury", isCorrect: false },
        { id: 3, value: "Venus", isCorrect: false }
      ]
    },
    {
      id: 5,
      question: "Which planet is known for having a Great Red Spot?",
      imgUrl: "https://cdn-icons-png.flaticon.com/512/2949/2949022.png",
      choices: [
        { id: 1, value: "Mars", isCorrect: false },
        { id: 2, value: "Jupiter", isCorrect: true },
        { id: 3, value: "Saturn", isCorrect: false }
      ]
    }
  ]
}
